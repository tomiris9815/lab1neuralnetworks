using System;

class MainClass {
  public static void Main (string[] args) {
    
    double alpha=1;
    Console.WriteLine ("Enter the number of inputs:");
    int numberOfInputs=int.Parse(Console.ReadLine());
    double resultresult=0.0;
    double resSecondCase=0;
        for(int i=1;i<=numberOfInputs;i++){
          Console.WriteLine("Enter the value of input"+i+":");
          double x=double.Parse(Console.ReadLine());
          Console.WriteLine("Enter the weight of input"+i+":");
          double w=double.Parse(Console.ReadLine());
          resSecondCase=x*w;
          resultresult=resultresult+resSecondCase; 
        }
        Console.WriteLine("The sum of inputs: "+resultresult);

        Console.WriteLine("Press 1 if you want to calculate with bias");
        Console.WriteLine("Press 2 if you want to calculate without bias");
        int choice=int.Parse(Console.ReadLine());
        if(choice==1){
          double bias=1;
          double resWithBias=resultresult+bias*alpha;
          Console.WriteLine("The result with bias:"+resWithBias);
          double activationFunction =1.0 / (1.0 + Math.Pow(Math.E, -alpha*resWithBias));
          Console.WriteLine("Activation function" + activationFunction);
        }else if(choice==2){
          double res=resultresult;
          double activationFunction =1.0 / (1.0 + Math.Pow(Math.E, -alpha*res));
          Console.WriteLine(activationFunction);
        }
  }
  
}